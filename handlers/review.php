<?
	$error=0;
	$email_pattern="/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/";
	if(!preg_match($email_pattern, $_POST['email']) || strlen(trim($_POST['email']))==0) $error=1;
	if(strlen(trim($_POST['name']))==0) $error+=2;
	if(strlen(trim($_POST['mess']))==0) $error+=4;
	
	echo $error;
	if ( $error==0) {
		CModule::IncludeModule('iblock'); 
		$el = new CIBlockElement;

		$PROP = array();
		$PROP[53] = $_POST["email"]; 

		$arLoadProductArray = Array(
		  "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
		  "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
		  "IBLOCK_ID"      => 5,
		  "PROPERTY_VALUES"=> $PROP,
		  "NAME"           => $_POST["name"],
		  "ACTIVE"         => "N",            // активен
		  "PREVIEW_TEXT"   => $_POST["mess"]
		  );

		if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
			//echo "New ID: ".$PRODUCT_ID;
			$arEventFields= array(
				"EMAIL" => $_POST["email"],
				"NAME" => $_POST["name"],
				"MESSAGE" => $_POST["mess"],
				"REVIEW" => $PRODUCT_ID
			);
			CEvent::Send("REVIEW", "s1", $arEventFields, "N", 42);
			//CEvent::Send("REVIEW", "s1", $arEventFields, "N", 39);
		} else {
			//echo "Error: ".$el->LAST_ERROR;
		}
	}
	
	die();
?>
