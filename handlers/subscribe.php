<?//if ($_POST["fullname"] && $_POST["email"] && $_POST["phone"] && $_POST["comment"] && $_POST["day"] && $_POST["month"] && $_POST["hour"] && $_POST["minute"] && $_POST["service"]):?>
<?
	try {
		if (!CModule::IncludeModule('subscribe')) return;
		//there must be at least one newsletter category
		$RUB_ID[]=1;

		if($strWarning == "")
		{
			$arFields = Array(
				"USER_ID" => ($USER->IsAuthorized()? $USER->GetID():false),
				"FORMAT" => ($FORMAT <> "html"? "text":"html"),
				"EMAIL" => $_POST["email"],
				"ACTIVE" => "Y",
				"RUB_ID" => $RUB_ID
			);
			$subscr = new CSubscription;

			//can add without authorization
			$ID = $subscr->Add($arFields);
			if($ID>0)
				CSubscription::Authorize($ID);
			else
				$strWarning .= $subscr->LAST_ERROR;
		}
		echo $strWarning;
		
		die();
	}
	catch (Exception $e) {
		echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
	}
?>
<?//endif;?>
