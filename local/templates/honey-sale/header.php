<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);
?>
    <html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?PHP header("Content-Type: text/html; charset=utf-8"); ?>
        <? $APPLICATION->ShowMeta("keywords") ?>
        <? $APPLICATION->ShowMeta("description") ?>
        <? $APPLICATION->ShowHead(); ?>
        <title><? $APPLICATION->ShowTitle() ?></title>

        <meta property="og:title" content="<? $APPLICATION->ShowTitle() ?> "/>
        <meta property="og:description" content="<? $APPLICATION->ShowTitle("description") ?>"/>
        <meta property="og:image" content="<?= SITE_TEMPLATE_PATH ?>/images/logo.png"/>
        <meta property="og:url" content=""/>
        <meta name="viewport" content="width=1000, initial-scale=1.0">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <link rel="icon" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon.ico">
        <!-- Bootstrap core CSS -->
        <link href="<?= SITE_TEMPLATE_PATH ?>/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->

        <link href="<?= SITE_TEMPLATE_PATH ?>/css/template.css" rel="stylesheet">
        <link href="/_css/style.css" rel="stylesheet">
        <link href="<?= SITE_TEMPLATE_PATH ?>/css/responsive.css" rel="stylesheet">
        <link href="<?= SITE_TEMPLATE_PATH ?>/css/jquery-ui.css" rel="stylesheet">
        <link href="<?= SITE_TEMPLATE_PATH ?>/css/jquery-ui.structure.css" rel="stylesheet">
        <link href="<?= SITE_TEMPLATE_PATH ?>/css/jquery-ui.theme.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Bootstrap core JavaScript
       ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <!-- Add jQuery library -->
        <script src="<?= SITE_TEMPLATE_PATH ?>/js/bootstrap.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="<?= SITE_TEMPLATE_PATH ?>/js/ie10-viewport-bug-workaround.js"></script>
        <script src="<?= SITE_TEMPLATE_PATH ?>/js/livequery.js"></script>
        <script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery-ui.js"></script>
        <script src="/_js/script.js"></script>
        <!-- Add fancyBox -->
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/js/fancybox/source/jquery.fancybox.css?v=2.1.5"
              type="text/css" media="screen"/>
        <script type="text/javascript"
                src="<?= SITE_TEMPLATE_PATH ?>/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>


        <link rel="stylesheet"
              href="<?= SITE_TEMPLATE_PATH ?>/js/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7"
              type="text/css" media="screen"/>
        <script type="text/javascript"
                src="<?= SITE_TEMPLATE_PATH ?>/js/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

    </head>

<body>
<? $APPLICATION->ShowPanel(); ?>
    <div class="popup-window-overlay-me" id="popup-window-overlay-CatalogElementBasket_bx_117848907_3027"
         style="z-index: 999; width: 100%; height: 100%; display: none; position:fixed;"></div>
<div class="wrapper">
    <header class="header">
        <div class="headerMenu">
            <div class="container">
                <? $APPLICATION->IncludeComponent("bitrix:menu", "top", Array(
                        "ROOT_MENU_TYPE" => "top",
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "top",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "Y",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => ""
                    )
                ); ?>
                <ul class="personalBlock">
                    <? if (!$USER->IsAuthorized()) { ?>
                        <li><a href="#" data-toggle="modal" data-target="#loginModal"><span
                                    class="login-icon header-icon"></span>Войти</a></li>
                        <li><a href="/registration/"><span class="registration-icon header-icon "></span>Регистрация</a>
                        </li>
                    <? } else { ?>
                        <li><a href="/personal/"><span class="personal-icon header-icon"></span>Личный
                                кабинет</a></li>
                        <li><a href="/?logout=yes"><span class="logout-icon header-icon"></span>Выйти</a>
                        </li>
                    <? } ?>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="subHeader">
                <div class="hs-logo">
                    <a href="/"><img src="/_images/logo.png" alt=""></a>
                </div>
                <div class="hs-headerSearch">
                    <? $APPLICATION->IncludeComponent("bitrix:search.suggest.input", "", Array(
                            "NAME" => "search1",
                            "VALUE" => "Поиск",
                            "INPUT_SIZE" => "60",
                            "DROPDOWN_SIZE" => "10"
                        )
                    ); ?>
                </div>
                <div class="hs-headerRecall">
                    <div class="hs-phone">8 (494) 231 50 12</div>
                    <div class="hs-ajax-link">Заказать звонок</div>
                </div>
                <div class="hs-headerCart">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:sale.basket.basket.line",
                        "menu_cart",
                        Array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
                            "SHOW_NUM_PRODUCTS" => "Y",
                            "SHOW_TOTAL_PRICE" => "Y",
                            "SHOW_EMPTY_VALUES" => "Y",
                            "SHOW_PERSONAL_LINK" => "N",
                            "PATH_TO_PERSONAL" => SITE_DIR . "personal/",
                            "SHOW_AUTHOR" => "N",
                            "PATH_TO_REGISTER" => SITE_DIR . "login/",
                            "PATH_TO_PROFILE" => SITE_DIR . "personal/",
                            "SHOW_PRODUCTS" => "N",
                            "POSITION_FIXED" => "N"
                        )
                    ); ?>
                </div>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="hs-blackCatalogMenu">

            <? $APPLICATION->IncludeComponent("bitrix:menu", "blackCatalog", Array(
                    "ROOT_MENU_TYPE" => "blackCatalog",
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "Y",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => ""
                )
            ); ?>
        </div>
    </div>
<?
if ($APPLICATION->GetCurPage(false) !== '/' && $APPLICATION->GetCurPage(false) !== '' && $APPLICATION->GetCurPage(false) !== '/index.php') {
    ?>
    <div class="main-cnt">
    <div class="container">
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="main-cnt-inner "></div>
    <div class="page-cnt">
    <script>
        $(document).ready(function () {
            $(".header").addClass("inheader");
        });
    </script>

    <?
    if (strpos($APPLICATION->GetCurPage(false), '/catalog/') === false && $APPLICATION->GetCurPage(false) !== '/catalog/') {
        ?>
        <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "breads", Array(
        "COMPONENT_TEMPLATE" => ".default",
        "START_FROM" => "0",    // Номер пункта, начиная с которого будет построена навигационная цепочка
        "PATH" => "",    // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
        "SITE_ID" => "s1",    // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
    ),
        false
    ); ?>

        <h1><? $APPLICATION->ShowTitle() ?></h1>
    <?
    } else {
    ?>
    <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "breads_catalog", Array(
        "COMPONENT_TEMPLATE" => ".default",
        "START_FROM" => "0",    // Номер пункта, начиная с которого будет построена навигационная цепочка
        "PATH" => "",    // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
        "SITE_ID" => "s1",    // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
    ),
        false
    ); ?>
        <script>
            $(document).ready(function () {
                $("#bx_breadcrumb_0").remove();
                $("#bx_breadcrumb_0l").remove();
            })
        </script>
    <? } ?>
    <?
}
?>