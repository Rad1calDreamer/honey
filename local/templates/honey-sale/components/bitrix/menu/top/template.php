<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true); ?>

<? if (!empty($arResult)): ?>
    <div class="topMenuContainer">
        <ul class="topMenu">


            <?
            $previousLevel = 0;
            foreach ($arResult as $arItem):?>


                <? if ($arItem["PERMISSION"] > "D"): ?>

                    <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
                        <li class="<? if ($arItem["SELECTED"]): ?>root-item-selected<? else: ?>root-item<? endif ?>"><a
                                href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
                    <? else: ?>
                        <li<? if ($arItem["SELECTED"]): ?> class="item-selected"<? endif ?>><a
                                href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
                    <? endif ?>

                <? else: ?>

                    <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
                        <li><a href="" title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>"><?= $arItem["TEXT"] ?></a>
                        </li>
                    <? else: ?>
                        <li class="denied"><a href=""
                                              title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>"><?= $arItem["TEXT"] ?></a>
                        </li>
                    <? endif ?>

                <? endif ?>


            <? endforeach ?>


        </ul>
    </div>
<? endif ?>