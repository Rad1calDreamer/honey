<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-5 col-xs-12">
			<div class="border-avatar entity-center margin-bottom-xs">
			<?if(is_array($arItem["PREVIEW_PICTURE"])) {?>
				<img
					class="center-block img-responsive"
					border="0"
					src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
					width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
					height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
					alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
					title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
					style="float:left"
				/>
			<? } else { ?>
				<img
					class="center-block img-responsive"
					border="0"
					src="<?=SITE_TEMPLATE_PATH?>/images/no-ava.png"
					alt=""
					title=""
					style="float:left"
				/>
				
			<? } ?>
			</div>
		</div>
		<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
			<div class="autor-comment text-center-sm">
				 <?echo $arItem["NAME"]?>
			</div>
			<div class="text-comment">
				<p><?echo $arItem["PREVIEW_TEXT"];?></p>
			</div>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
		</div>
	</div>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
