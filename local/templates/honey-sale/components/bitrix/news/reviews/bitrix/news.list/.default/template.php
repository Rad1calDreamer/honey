<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="news-item review_item row" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="left_side_review col-lg-3 col-md-3 col-sm-4 col-xs-12">
			<?if(is_array($arItem["PREVIEW_PICTURE"])) {?>
				<img
					class="preview_picture img-responsive"
					border="0"
					src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
					width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
					height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
					alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
					title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
					style="float:left"
				/>
			<? } else { ?>
				<img
					class="preview_picture img-responsive"
					border="0"
					src="<?=SITE_TEMPLATE_PATH?>/images/no-ava.png"
					alt=""
					title=""
					style="float:left"
				/>
				
			<? } ?>
		</div>
		<div class="right_side_review col-lg-9 col-md-9 col-sm-8 col-xs-12">
			<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
				<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
					<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><div class="title_review"><?echo $arItem["NAME"]?></div></a>
				<?else:?>
					<div class="title_review"><?echo $arItem["NAME"]?></div>
				<?endif;?>
			<?endif;?>
			<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
				<div class="text_review"><?echo $arItem["PREVIEW_TEXT"];?></div>
			<?endif;?>
			<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
				<div style="clear:both"></div>
			<?endif?>
			<? if ($arItem["DISPLAY_PROPERTIES"]["ORIGINAL"]["FILE_VALUE"]["SRC"]) { ?>
				<div class="original">
					<span class="glyphicon glyphicon-file"></span>
					<a href="<?=$arItem["DISPLAY_PROPERTIES"]["ORIGINAL"]["FILE_VALUE"]["SRC"]?>" target="_blank">Скачать оригинал отзыва</a>
				</div>
			<? } ?>
		</div>
	</div>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
