<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="bx_cats_title">Оставьте свой отзыв</div>
		<form class="form-inline" id="review_form" role="form">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="w250 form-group">
						<input type="text" class="form-control review_name" id="reviewInputName" placeholder="Введите ваше имя...">
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="w250 form-group">
						<input type="email" class="form-control review_email" id="reviewInputEmail" placeholder="Введите ваш email...">
					</div>	
				</div>
			</div>	
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">		
					<div class="w250 form-group">
						<textarea name="text" class="form-control review_phone" id="reviewInputMess" rows="6" placeholder="Введите Ваш отзыв..."></textarea>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
					<div class="w250 form-group center_a">
						<button type="button" class="btn btn-success review_btn">Отправить</button>
					</div>
				</div>
			</div>
		</form>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" class="center_a">	
				<div class="thanks_review" style="display:none">Спасибо, Ваша отзыв отправлен.</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready( function (){
		$(".review_btn").click( function (){
			
			var email=$("#reviewInputEmail").val(),
				name=$("#reviewInputName").val(),
				mess=$("#reviewInputMess").val();
				
			$("#reviewInputEmail").removeClass('error_input');
			$("#reviewInputName").removeClass('error_input');
			$("#reviewInputMess").removeClass('error_input');
				
			$.ajax({
				url: "/ajax.handler.php",
				type: "POST",
				data: "PAGE=review&email="+email+"&name="+name+"&mess="+mess,
				dataType:"html",
				async: false,
				success: function(data){
					switch (parseInt(data)) {
						case 0: {
							$("#reviewInputName").val('');
							$("#reviewInputEmail").val('');
							$("#reviewInputMess").text('');
							$("#reviewInputMess").val('');
							$(".thanks_review").show('slow');
							setTimeout("window.location=window.location.pathname;",3500);
							break;
						}
						case 1: {
							$("#reviewInputEmail").addClass('error_input');
							break;
						}
						case 2: {
							$("#reviewInputName").addClass('error_input');
							break;
						}
						case 3: {
							$("#reviewInputName").addClass('error_input');
							$("#reviewInputEmail").addClass('error_input');
							break;
						}
						case 4: {
							$("#reviewInputMess").addClass('error_input');
							break;
						}
						case 5: {
							$("#reviewInputEmail").addClass('error_input');
							$("#reviewInputMess").addClass('error_input');
							break;
						}
						case 6: {
							$("#reviewInputName").addClass('error_input');
							$("#reviewInputMess").addClass('error_input');
							break;
						}
						case 7: {
							$("#reviewInputName").addClass('error_input');
							$("#reviewInputEmail").addClass('error_input');
							$("#reviewInputMess").addClass('error_input');
							break;
						}
					}
				},
				error: function(jqxhr, status, errorMsg) {
					alert(jqxhr+"  "+status+"  "+errorMsg);
				}
		
			});	
		});
	});
</script>