<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

<? if ($arParams['SHOW_AUTHOR'] == 'Y'): ?>
    <div class="bx-basket-block">
        <i class="fa fa-user"></i>
        <? if ($USER->IsAuthorized()):
            $name = trim($USER->GetFullName());
            if (!$name)
                $name = trim($USER->GetLogin());
            if (strlen($name) > 15)
                $name = substr($name, 0, 12) . '...';
            ?>
            <a href="<?= $arParams['PATH_TO_PROFILE'] ?>"><?= $name ?></a>
            &nbsp;
            <a href="?logout=yes"><?= GetMessage('TSB1_LOGOUT') ?></a>
        <? else: ?>
            <a href="<?= $arParams['PATH_TO_REGISTER'] ?>?login=yes"><?= GetMessage('TSB1_LOGIN') ?></a>
            &nbsp;
            <a href="<?= $arParams['PATH_TO_REGISTER'] ?>?register=yes"><?= GetMessage('TSB1_REGISTER') ?></a>
        <? endif ?>
    </div>
<? endif ?>
<div class="hs-carticon <?= ($arResult['NUM_PRODUCTS'] ==0) ? 'empty' : ''; ?>" data-count="<?= ($arResult['NUM_PRODUCTS'] > 0) ? $arResult['NUM_PRODUCTS'] : ''; ?>"></div>
<? if ($arResult['NUM_PRODUCTS'] == 0) {
    echo "<div class=\"hs-headerCartTitle\">Нет товаров</div>";
} else { ?>
    <a href="/personal/cart">
        <? if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y')): ?>
            <div class="hs-headerCartTitle">Товаров на сумму</div>
        <? endif ?>
        <div class="hs-headerCartSumm">
            <?= $arResult['TOTAL_PRICE'] ?>
        </div>
    </a>
<? } ?>

<? if ($arParams['SHOW_PERSONAL_LINK'] == 'Y'): ?>
    <br>
    <span class="icon_info"></span>
    <a href="<?= $arParams['PATH_TO_PERSONAL'] ?>"><?= GetMessage('TSB1_PERSONAL') ?></a>

<? endif ?>
