<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<script>
	$(document).ready( function (){
		$(".accordion-heading").click( function (){
			if ($(this).hasClass("active")) $(this).removeClass("active");
			else $(this).addClass("active");
		});
	})
</script>
<div class="news-list accs_faq">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<div class="accordion" id="accordion2">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		
		<div class="accordion-group">
			<div class="accordion-heading">
				<a class="accordion-toggle" data-toggle="collapse" href="#collapse<?=$arItem["ID"];?>">
					<span class="glyphicon glyphicon-question-sign"></span> <?=$arItem["PREVIEW_TEXT"];?>
				</a>
			</div>
			<div id="collapse<?=$arItem["ID"];?>" class="accordion-body collapse">
				<div class="accordion-inner">
					<?=$arItem["DETAIL_TEXT"];?>
				</div>
			</div>
		</div>
	<?endforeach;?>
</div>



<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
