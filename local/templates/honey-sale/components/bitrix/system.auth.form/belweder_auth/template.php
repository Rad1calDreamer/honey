<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode(false); ?>

<?
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
	ShowMessage($arResult['ERROR_MESSAGE']);
?>

<?if($arResult["FORM_TYPE"] == "login"):?>

<div id="modal-login">
	<div id="form_login">
		<form action="<?=$arResult["AUTH_URL"]?>" name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" class="form-inline form-login frm frm_stacked">
			<?if($arResult["BACKURL"] <> ''):?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
			<?endif?>
			<?foreach ($arResult["POST"] as $key => $value):?>
				<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
			<?endforeach?>
			<input type="hidden" name="AUTH_FORM" value="Y" />
			<input type="hidden" name="TYPE" value="AUTH" />
			<div class="form-group ">
				<input type="text" id="login-modal" name="USER_LOGIN" placeholder="Логин" class="login_input form-control" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" autofocus="" autocapitalize="off" autocorrect="off">
			</div>
			<div class="form-group ">
				<input type="password" id="password" class="login_input form-control" placeholder="Пароль" name="USER_PASSWORD">
				<?if($arResult["SECURE_AUTH"]):?>
					<span class="bx-auth-secure" id="bx_auth_secure<?=$arResult["RND"]?>" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
						<div class="bx-auth-secure-icon"></div>
					</span>
					<noscript>
					<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
						<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
					</span>
					</noscript>
					<script type="text/javascript">
					document.getElementById('bx_auth_secure<?=$arResult["RND"]?>').style.display = 'inline-block';
					</script>
				<?endif?>
			</div>
			<div class="btn_block">
				
				<input type="submit" id="login_submit" name="login_submit" value="Войти" class="btn btn-success bx_filter_search_button"/>
			</div>
		</form>
		<p class="txt_c txt_neutral pb20">или<br/>авторизуйтесь<br/>через соц.сети</p>
		<div class="row thingutter">
			<?
				$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "belweder_soc_auth", 
					array(
						"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
						"SUFFIX"=>"form",
					), 
					$component, 
					array("HIDE_ICONS"=>"Y")
				); 
			?>
		</div>
	</div>
	<hr class="hr_line">
	<div>
		<a class="bx_filter_search_reset neutral_link right lh_h5 fsbase" href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow">Забыли пароль?</a> 
	</div>
</div>

<?endif?>