<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$INPUT_ID = trim($arParams["~INPUT_ID"]);
if(strlen($INPUT_ID) <= 0)
	$INPUT_ID = "title-search-input";
$INPUT_ID = CUtil::JSEscape($INPUT_ID);

$CONTAINER_ID = trim($arParams["~CONTAINER_ID"]);
if(strlen($CONTAINER_ID) <= 0)
	$CONTAINER_ID = "title-search";
$CONTAINER_ID = CUtil::JSEscape($CONTAINER_ID);

if($arParams["SHOW_INPUT"] !== "N"):?>
<script>
$(document).ready( function (){
	if (screen.width<768) {
		$(".search_large").remove();
	} 
	else {
		$(".search_small").remove();
	}
})
</script>
<div id="<?echo $CONTAINER_ID?>" class="row bx_search_container hs-headerSearchContainer">
	<form action="<?echo $arResult["FORM_ACTION"]?>">
			<input class="col-lg-11 col-md-11 col-sm-9 col-xs-12 input-search-catalog visible-lg visible-md visible-sm hidden-xs search_large" id="<?echo $INPUT_ID?>" type="text" name="q" placeholder="Введите название товара..." value="" autocomplete="off">
			<input class="col-lg-11 col-md-11 col-sm-9 col-xs-12 input-search-catalog hidden-lg hidden-md hidden-sm visible-xs search_small" id="<?echo $INPUT_ID?>" type="text" name="q" placeholder="Поиск..." value="" autocomplete="off">
									
			<input name="s" type="submit" class="col-lg-1  col-md-1 col-sm-3  col-xs-12 btn-search" value="Поиск"/>
	</form>
</div>
<?endif?>
<script>
	BX.ready(function(){
		new JCTitleSearch({
			'AJAX_PAGE' : '<?echo CUtil::JSEscape(POST_FORM_ACTION_URI)?>',
			'CONTAINER_ID': '<?echo $CONTAINER_ID?>',
			'INPUT_ID': '<?echo $INPUT_ID?>',
			'MIN_QUERY_LEN': 2
		});
	});
</script>

