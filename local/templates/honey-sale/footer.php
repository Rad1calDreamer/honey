<?
	if ($APPLICATION->GetCurPage(false) !== '/' && $APPLICATION->GetCurPage(false) !== '' && $APPLICATION->GetCurPage(false) !== '/index.php') {
		$inner_class="inner-footer";
?>		</div>
		</div>
		</div>
		</div>
		</div>
<?
	}
?>
<footer>
				<div class="footer-box bg-site <?=$inner_class?>">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<span class="corner-left corner-footer"></span>
								<span class="corner-right corner-footer"></span>
								<div class="seo-text">
									<span class="corner-left-inner corner-footer"></span>
									<span class="corner-right-inner corner-footer"></span>
								</div>
								<div class="contact-footer">
										<div class="row text-center-sm">
											<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
												<div data-toggle="modal" data-target=".map-modal"  href="#" class="title-c dashed">Показать нас на карте</div>
												<p>Россия, 156000<br>город Кострома, ул. Ленина, д.8
											</div>
											<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
												<div data-toggle="modal" data-target=".message-modal"  class="title-c dashed">Отправить сообщение</div>
												<p>info@honey-sale.ru<br>shop@honey-sale.ru
											</div>
											<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
												<div data-toggle="modal" data-target=".callback-modal" class="title-c dashed">Заказать звонок</div>
												<p>+7 (4942) 31-50-12<br>+7 (4942) 31-50-12
											</div>
											<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
												<div  class="title-c no-dashed">Мы в соц.сетях</div>
												<p>
												<a href="http://vk.com" target="_blank"><img class="social_btn img-responsive" src="<?=SITE_TEMPLATE_PATH?>/images/vk.png" alt="" title="Мы в ВКонтакте"></a>
												<a href="http://facebook.com" target="_blank"><img class="social_btn img-responsive" src="<?=SITE_TEMPLATE_PATH?>/images/facebook.png" alt="" title="Мы в Facebook"></a>
												<a href="http://twitter.com" target="_blank"><img class="social_btn img-responsive" src="<?=SITE_TEMPLATE_PATH?>/images/twitter.png" alt="" title="Мы в Twitter"></a>
												</p>
											</div>
										</div>
								</div>
								<div class="legal text-center-sm">
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
												<p>© 2015 Магазин «Пчеловодство». Все права защищены<br>Вся информация на сайте носит исключительно справочный характер<br>и ни при каких условиях не является публичной офертой.</p>
										        <p>Индивидуальный предприниматель Кубылькина Светлана Анатольевна<br>
										       ИНН 440109513430</p>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
												<p class="text-splash entity-margin-top-bottom">Разработка сайта <a target="_blank" href="http://splash-studio.ru"><span class="splash"></span></a></p>
												<p  id="bx-composite-banner"></p>
											</div>
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal fade callback-modal bs-example-modal-sm page-container" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-sm">
					<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					 <div class="title-con">Закажите звонок</div>
					 <div class="subtitle-con">и мы перезвоним вам <br>в ближайшеее время</div>
					<form class="form-inline" id="call_form" role="form">
						<div class="w250 form-group">
							<input type="text" class="form-control call_name" id="callInputName" placeholder="Введите ваше имя...">
						</div>
						<div class="w250 form-group">
							<input type="text" class="form-control call_phone" id="callInputPhone" placeholder="Введите ваш телефон...">
						</div>
						<div class="w250 form-group">
							<button type="button" class="btn btn-success call_btn">Оставить заявку</button>
						</div>
					</form>
					<div class="thanks_call" style="display:none">Спасибо, Ваша заявка отправлена.</div>
					</div>
				  </div>
				</div>


				<div class="modal fade message-modal bs-example-modal-sm page-container" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-sm">
					<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					 <div class="title-con">Ваше сообщение</div>
					 <div class="subtitle-con">и мы ответим вам <br>в ближайшеее время</div>
					<form class="form-inline" id="message_form" role="form">
						<div class="w250 form-group">
							<input type="text" class="form-control message_name" id="messageInputName" placeholder="Введите ваше имя...">
						</div>
						<div class="w250 form-group">
							<input type="text" class="form-control message_email" id="messageInputEmail" placeholder="Введите ваш email...">
						</div>		
						<div class="w250 form-group">
							<textarea name="text" class="form-control message_phone" id="messageInputPhone" placeholder="Введите ваше сообщение..."></textarea>
						</div>
						<div class="w250 form-group">
							<button type="button" class="btn btn-success message_btn">Отправить</button>
						</div>
					</form>
					<div class="thanks_message" style="display:none">Спасибо, Ваша заявка отправлена.</div>
					</div>
				  </div>
				</div>

				<div class="modal fade map-modal bs-example-modal-sm page-container" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-lg">
					<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					 <div class="title-con">Схема проезда</div>
					  <div class="subtitle-con">наше месторасположение<br>и график работы</div>
					<a class="dg-widget-link" href="http://2gis.ru/kostroma/firm/4785602885097622/center/40.92893899999998,57.769947000000016/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Костромы</a><div class="dg-widget-link"><a href="http://2gis.ru/kostroma/center/40.928939,57.769947/zoom/16/routeTab/rsType/bus/to/40.928939,57.769947╎Пчеловодство, магазин?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Пчеловодство, магазин</a></div><script charset="utf-8" src="http://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":640,"height":600,"borderColor":"#a3a3a3","pos":{"lat":57.769947000000016,"lon":40.92893899999998,"zoom":16},"opt":{"city":"kostroma"},"org":[{"id":"4785602885097622"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
					</div>
				  </div>
				</div>

				<!-- Login Modal -->
				<div class="modal fade page-container" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-sm">
					<div class="modal-content">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<div class="title-con">Авторизация</div>
					  <div class="modal-body">
					  <?
						\Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("login");
					  ?>
					  <?$APPLICATION->IncludeComponent(
							"bitrix:system.auth.form",
							"belweder_auth",
							Array(
								"COMPONENT_TEMPLATE" => "belweder_auth",
								"REGISTER_URL" => SITE_DIR."login/",
								"FORGOT_PASSWORD_URL" => "",
								"PROFILE_URL" => SITE_DIR."personal/",
								"SHOW_ERRORS" => "Y"
							)
						);?>
						<? \Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("login", "Загрузка..."); ?>
					  </div>
					</div>
				  </div>
				</div>
				<div id="toTop"><span class="glyphicon glyphicon-chevron-right"></span></div>	
			</footer>
			 
		</div>
		<!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter34354485 = new Ya.Metrika({ id:34354485, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/34354485" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
<script type="text/javascript" src="//cdn.callbackhunter.com/cbh.js?hunter_code=73aaec0a7fe0ad92f02434dd2a4331e4" charset="UTF-8"></script>
	</body>
</html>
	<script>
		$(document).ready( function (){
			/* for (var i=1; i<2000; i=i+10) {
				setTimeout("$(window).resize()", i);
			}
			$(window).resize(function(){
				console.log("1");
			}) */
			$("#title-search").width($("#title-search").width());
			$(".sort_select").selectmenu();
			$('.fancybox').fancybox();
			$(".addCartBtn").click( function (){
				var pid=$(this).attr("pid"),
					thnks=$(this).parent().find(".thanks_message_");
				$.ajax({
					url: "/ajax.handler.php",
					type: "POST",
					data: "PAGE=add_cart&pid="+pid,
					dataType:"html",
					async: false,
					success: function(data){
						thnks.show();
						$(".popup-window-overlay-me").show();
						BX.onCustomEvent('OnBasketChange');
					},
					error: function(jqxhr, status, errorMsg) {
						alert(jqxhr+"  "+status+"  "+errorMsg);
					}
			
				});	
			});
			$(".closer").click( function (){
				$(this).parents(".thanks_message_").hide();
				$(".popup-window-overlay-me").hide();
			});
			
			
			
			$(".prev_text_abs").fadeOut();
			$('.grey').fadeOut(0);
			$(".product-cnt").hover(function(){
				//$(this).find(".prev_text_abs").fadeIn("slow");
				$(this).find(".product-title").css("color", "#844220");
				$(this).find('.yellow').fadeOut(200);
				//$(this).find('.product-img img').fadeOut(200);
				$(this).parent().find('.grey').fadeIn(200);
			}, function(){
				//$(this).find(".prev_text_abs").fadeOut("slow");
				$(this).find(".product-title").css("color", "black");
				$(this).find('.yellow').fadeIn(200);
				$(this).find('.grey').fadeOut(200); 
				//$(this).find('.product-img img').fadeIn(400);
			});
			$(".message_btn").click( function (){
				
				var email=$("#messageInputEmail").val(),
					name=$("#messageInputName").val(),
					message=$("#messageInputPhone").val();
				
				if (name=="" || name.replace(/\s*/g,'')=="") {
					$("#messageInputName").addClass("error_input");
				}
				else {
					$("#messageInputName").removeClass("error_input");
				}
				
				if (email=="" || email.replace(/\s*/g,'')=="") {
					$("#messageInputEmail").addClass("error_input"); 
				}
				else {
					var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
					if (!pattern.test(email)){
						$("#messageInputEmail").addClass("error_input"); 
					} 
					else {
						$("#messageInputEmail").removeClass("error_input"); 
					}
				}
				
				if (message=="" || message.replace(/\s*/g,'')=="") {
					$("#messageInputPhone").addClass("error_input");
				}
				else {
					$("#messageInputPhone").removeClass("error_input");
				}
				
				if (!$(".error_input")[0]) {
					$.ajax({
						url: "/ajax.handler.php",
						type: "POST",
						data: "PAGE=message&email="+email+"&name="+name+"&message="+message,
						dataType:"html",
						async: false,
						success: function(data){
							$("#messageInputEmail").val('');
							$("#messageInputName").val('');
							$("#messageInputPhone").val('');
							//$("#message_form").hide('slow');
							$(".thanks_message").show('slow');
						},
						error: function(jqxhr, status, errorMsg) {
							alert(jqxhr+"  "+status+"  "+errorMsg);
						}
				
					});	
				}
			});
			$(".call_btn").click( function (){
				
				var name=$("#callInputName").val(),
					phone=$("#callInputPhone").val();
				
				if (name=="" || name.replace(/\s*/g,'')=="") {
					$("#callInputName").addClass("error_input");
				}
				else {
					$("#callInputName").removeClass("error_input");
				}
				
				if (phone=="" || phone.replace(/\s*/g,'')=="") {
					$("#callInputPhone").addClass("error_input"); 
				}
				else {
					var pattern = /^[0-9()\-+ ]+$/;
					if (!pattern.test(phone)){
						$("#callInputPhone").addClass("error_input"); 
					} 
					else {
						$("#callInputPhone").removeClass("error_input"); 
					}
				}
				
				if (!$(".error_input")[0]) {
					$.ajax({
						url: "/ajax.handler.php",
						type: "POST",
						data: "PAGE=call&name="+name+"&phone="+phone,
						dataType:"html",
						async: false,
						success: function(data){
							$("#callInputName").val('');
							$("#callInputPhone").val('');
							//$("#call_form").hide('slow');
							$(".thanks_call").show('slow');
						},
						error: function(jqxhr, status, errorMsg) {
							alert(jqxhr+"  "+status+"  "+errorMsg);
						}
				
					});	
				}
			});
			
			$(".product-item-img").each(function(){
				$(this).height($(this).find(".yellow").height());
			});
			setTimeout('$(".product-item-img").each(function(){$(this).height($(this).find(".yellow").height());});', 2000);
			$(window).resize(function(){
				$(".product-item-img").each(function(){
					$(this).height($(this).find(".yellow").height());
				});
			});
			
			$(window).scroll(function() {
				if($(this).scrollTop() != 0) {
					$('#toTop').fadeIn();
				} else {
					$('#toTop').fadeOut();
				}
			});
			$('#toTop').click(function() {
				$('body,html').animate({scrollTop:0},800);
			});
		})
	</script>