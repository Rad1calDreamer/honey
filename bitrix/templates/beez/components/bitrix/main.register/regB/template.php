<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$this->setFrameMode(true);
?>
 

<div class="bx-auth-reg main_reg">

<?if($USER->IsAuthorized()):?>

<p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>

<?else:?>
<script>
	$(document).ready( function (){
		$("#sendButton2").click( function (){
			var email=$("#EMAIL2").val(),
				phone=$("#PERSONAL_PHONE2").val(),
				login=$("#LOGIN2").val(),
				fio=$("#NAME2").val(),
				password1=$("#PASSWORD2").val(),
				password2=$("#CONFIRM_PASSWORD2").val();
			
			$("#PASSWORD2").removeClass("error_input");
			$("#CONFIRM_PASSWORD2").removeClass("error_input");
			$("#PASSWORD2").removeAttr("title");
			$("#CONFIRM_PASSWORD2").removeAttr("title");
			
			if (password1!="" || password1.replace(/\s*/g,'')!="") {
				if (password1.length<6){
					$("#PASSWORD2").addClass("error_input");
					$("#CONFIRM_PASSWORD2").addClass("error_input");
					$("#PASSWORD2").attr("title", "Слишком короткий пароль. Минимум 6 символов");
					$("#CONFIRM_PASSWORD2").attr("title", "Слишком короткий пароль. Минимум 6 символов");
				}
				if (password1.length>20){
					$("#PASSWORD2").addClass("error_input");
					$("#CONFIRM_PASSWORD2").addClass("error_input");
					$("#PASSWORD2").attr("title", "Слишком длинный пароль. Максимум 20 символов");
					$("#CONFIRM_PASSWORD2").attr("title", "Слишком длинный пароль. Максимум 20 символов");
				}
				if (password1!=password2) {
					$("#PASSWORD2").addClass("error_input");
					$("#CONFIRM_PASSWORD2").addClass("error_input");
					$("#PASSWORD2").attr("title", "Пароли не совпадают");
					$("#CONFIRM_PASSWORD2").attr("title", "Пароли не совпадают");
				}
			}
			else {
				$("#PASSWORD2").addClass("error_input");
				$("#CONFIRM_PASSWORD2").addClass("error_input");
				$("#PASSWORD2").attr("title", "Введите пароль");
				$("#CONFIRM_PASSWORD2").attr("title", "Введите пароль");
			}

			$("#EMAIL2").removeClass("error_input");
			$("#EMAIL2").removeAttr("title");
			
			if (email=="" || email.replace(/\s*/g,'')=="") {
				$("#EMAIL2").addClass("error_input");
				$("#EMAIL2").attr("title", "Заполните это поле");
			}
			else {
				var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
				if (!pattern.test(email)){
					$("#EMAIL2").addClass("error_input");
					$("#EMAIL2").attr("title", "Некорректный email");
				} 
				$.ajax({
					url: "/ajax.handler.php",
					type: "POST",
					data: "PAGE=checkEmail&email="+email,
					dataType:"html",
					async: false,
					success: function(data){
						if (data!=0) {
							$("#EMAIL2").attr("title", "Этот email уже зарегестрирован на сайте");
							$("#EMAIL2").addClass("error_input");
						}
					},
					error: function(jqxhr, status, errorMsg) {
						alert(jqxhr+"  "+status+"  "+errorMsg);
					}
			
				});
			}
			
			if (phone=="" || phone.replace(/\s*/g,'')=="") {
				//$("#PERSONAL_PHONE2").addClass("error_input");
				//$("#PERSONAL_PHONE2").attr("title", "Заполните это поле");
				$("#PERSONAL_PHONE2").removeClass("error_input");
				$("#PERSONAL_PHONE2").removeAttr("title");
			}
			else {
				var pattern = /^[0-9()\-+ ]+$/;
				if (!pattern.test(phone)){
					$("#PERSONAL_PHONE2").addClass("error_input");
					$("#PERSONAL_PHONE2").attr("title", "Некорректный телефон");
				} 
				else {
					$("#PERSONAL_PHONE2").removeClass("error_input");
					$("#PERSONAL_PHONE2").removeAttr("title");
				}
			}
			
			if (fio=="" || fio.replace(/\s*/g,'')=="") {
				$("#NAME2").addClass("error_input");
				$("#NAME2").attr("title", "Заполните это поле");
			}
			else {
				$("#NAME2").removeClass("error_input");
				$("#NAME2").removeAttr("title");
			}
			
			$("#LOGIN2").removeClass("error_input");
			$("#LOGIN2").removeAttr("title");
			
			if (login=="" || login.replace(/\s*/g,'')=="") {
				$("#LOGIN2").addClass("error_input");
				$("#LOGIN2").attr("title", "Заполните это поле");
			}
			else {
				var pattern = /^[a-zA-Z0-9_]+$/;
				if (!pattern.test(login)){
					$("#LOGIN2").addClass("error_input");
					$("#LOGIN2").attr("title", "Некорректный логин");
				} 
				$.ajax({
					url: "/ajax.handler.php",
					type: "POST",
					data: "PAGE=checkLogin&login="+login,
					dataType:"html",
					async: false,
					success: function(data){
						if (data!=0) {
							$("#LOGIN2").attr("title", "Этот логин уже занят. Пожалуйста, используйте другой логин");
							$("#LOGIN2").addClass("error_input");
						}
					},
					error: function(jqxhr, status, errorMsg) {
						alert(jqxhr+"  "+status+"  "+errorMsg);
					}
			
				});
			}
			
			
			
			if (!$(".error_input")[0]) {
				$("#login_submit").click();
				
			}
		});
	});
</script>
<?
if (count($arResult["ERRORS"]) > 0):
	foreach ($arResult["ERRORS"] as $key => $error)
		if (intval($key) == 0 && $key !== 0) 
			$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);

	ShowError(implode("<br />", $arResult["ERRORS"]));

elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
?>
<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
<?endif?>

<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data"  class="frm frm_stacked">
<?
if($arResult["BACKURL"] <> ''):
?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
endif;
?>

<?foreach ($arResult["SHOW_FIELDS"] as $FIELD):?>
	<?if($FIELD == "AUTO_TIME_ZONE" && $arResult["TIME_ZONE_ENABLED"] == true):?>
		<tr>
			<td><?echo GetMessage("main_profile_time_zones_auto")?><?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?><span class="starrequired">*</span><?endif?></td>
			<td>
				<select name="REGISTER[AUTO_TIME_ZONE]" onchange="this.form.elements['REGISTER[TIME_ZONE]'].disabled=(this.value != 'N')">
					<option value=""><?echo GetMessage("main_profile_time_zones_auto_def")?></option>
					<option value="Y"<?=$arResult["VALUES"][$FIELD] == "Y" ? " selected=\"selected\"" : ""?>><?echo GetMessage("main_profile_time_zones_auto_yes")?></option>
					<option value="N"<?=$arResult["VALUES"][$FIELD] == "N" ? " selected=\"selected\"" : ""?>><?echo GetMessage("main_profile_time_zones_auto_no")?></option>
				</select>
			</td>
		</tr>
		<tr>
			<td><?echo GetMessage("main_profile_time_zones_zones")?></td>
			<td>
				<select name="REGISTER[TIME_ZONE]"<?if(!isset($_REQUEST["REGISTER"]["TIME_ZONE"])) echo 'disabled="disabled"'?>>
		<?foreach($arResult["TIME_ZONE_LIST"] as $tz=>$tz_name):?>
					<option value="<?=htmlspecialcharsbx($tz)?>"<?=$arResult["VALUES"]["TIME_ZONE"] == $tz ? " selected=\"selected\"" : ""?>><?=htmlspecialcharsbx($tz_name)?></option>
		<?endforeach?>
				</select>
			</td>
		</tr>
	<?else:?>
	<div class="frm_grp ">
		<label for="login-modal" class="login_label"><?=GetMessage("REGISTER_FIELD_".$FIELD)?>:<?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?><span class="starrequired">*</span><?endif?></label>
		<?
	switch ($FIELD)
	{
		case "PASSWORD":
			?><input size="30" type="password" id="<?=$FIELD?>2" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" autocomplete="off" class="bx-auth-input" />
<?if($arResult["SECURE_AUTH"]):?>
				<span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
				<noscript>
				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
				</noscript>
<script type="text/javascript">
document.getElementById('bx_auth_secure').style.display = 'inline-block';
</script>
<?endif?></div>
<?
			break;
		case "CONFIRM_PASSWORD":
			?><input size="30" type="password" id="<?=$FIELD?>2" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" autocomplete="off" /></div><?
			break;

		case "PERSONAL_GENDER":
			?><select name="REGISTER[<?=$FIELD?>]">
				<option value=""><?=GetMessage("USER_DONT_KNOW")?></option>
				<option value="M"<?=$arResult["VALUES"][$FIELD] == "M" ? " selected=\"selected\"" : ""?>><?=GetMessage("USER_MALE")?></option>
				<option value="F"<?=$arResult["VALUES"][$FIELD] == "F" ? " selected=\"selected\"" : ""?>><?=GetMessage("USER_FEMALE")?></option>
			</select><?
			break;

		case "PERSONAL_COUNTRY":
		case "WORK_COUNTRY":
			?><select name="REGISTER[<?=$FIELD?>]"><?
			foreach ($arResult["COUNTRIES"]["reference_id"] as $key => $value)
			{
				?><option value="<?=$value?>"<?if ($value == $arResult["VALUES"][$FIELD]):?> selected="selected"<?endif?>><?=$arResult["COUNTRIES"]["reference"][$key]?></option>
			<?
			}
			?></select><?
			break;

		case "PERSONAL_PHOTO":
		case "WORK_LOGO":
			?><input size="30" type="file" name="REGISTER_FILES_<?=$FIELD?>" /><?
			break;

		case "PERSONAL_NOTES":
		case "WORK_NOTES":
			?><textarea cols="30" rows="5" name="REGISTER[<?=$FIELD?>]"><?=$arResult["VALUES"][$FIELD]?></textarea><?
			break;
		default:
			if ($FIELD == "PERSONAL_BIRTHDAY"):?><small><?=$arResult["DATE_FORMAT"]?></small><br /><?endif;
			?><input size="30" type="text" id="<?=$FIELD?>2" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" /></div><?
				if ($FIELD == "PERSONAL_BIRTHDAY")
					$APPLICATION->IncludeComponent(
						'bitrix:main.calendar',
						'',
						array(
							'SHOW_INPUT' => 'N',
							'FORM_NAME' => 'regform',
							'INPUT_NAME' => 'REGISTER[PERSONAL_BIRTHDAY]',
							'SHOW_TIME' => 'N'
						),
						null,
						array("HIDE_ICONS"=>"Y")
					);
				?><?
	}?>
	<?endif?>
<?endforeach?>

<?// ********************* User properties ***************************************************?>
<?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
	<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
	<div class="frm_grp ">
	<label for="login-modal" class="login_label"><?=$arUserField["EDIT_FORM_LABEL"]?>:<?if ($arUserField["MANDATORY"]=="Y"):?><span class="starrequired">*</span><?endif;?></label>
			<?$APPLICATION->IncludeComponent(
				"bitrix:system.field.edit",
				$arUserField["USER_TYPE"]["USER_TYPE_ID"],
				array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"), null, array("HIDE_ICONS"=>"Y"));?>
	</div>
	<?endforeach;?>
<?endif;?>
<?// ******************** /User properties ***************************************************?>

<div class="row bottom_reg_block">
<div class="col-md-9 info_reg_block">
<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>
<p><span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?></p>
</div>
<div class="col-md-3 submit_reg_block">
<input type="button" name="register_submit_button" class="btn btn-success bx_filter_search_button" id="sendButton2" value="<?=GetMessage("AUTH_REGISTER")?>" />
<input type="submit" name="register_submit_button" style="display:none;" id="login_submit" value="<?=GetMessage("AUTH_REGISTER")?>" />

</div>
</div>

</form>
<?endif?>
</div>