<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
/* print("<pre>");
print_r($arResult["ID"]);
print("</pre>"); */

if (!empty($arResult['ITEMS']))
{
	$templateLibrary = array('popup');
	$currencyList = '';
	if (!empty($arResult['CURRENCIES']))
	{
		$templateLibrary[] = 'currency';
		$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
	}
	$templateData = array(
		'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
		'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
		'TEMPLATE_LIBRARY' => $templateLibrary,
		'CURRENCIES' => $currencyList
	);
	unset($currencyList, $templateLibrary);

	$arSkuTemplate = array();
	if (!empty($arResult['SKU_PROPS']))
	{
		foreach ($arResult['SKU_PROPS'] as &$arProp)
		{
			$templateRow = '';
			if ('TEXT' == $arProp['SHOW_MODE'])
			{
				if (5 < $arProp['VALUES_COUNT'])
				{
					$strClass = 'bx_item_detail_size full';
					$strWidth = ($arProp['VALUES_COUNT']*20).'%';
					$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
					$strSlideStyle = '';
				}
				else
				{
					$strClass = 'bx_item_detail_size';
					$strWidth = '100%';
					$strOneWidth = '20%';
					$strSlideStyle = 'display: none;';
				}
				$templateRow .= '<div class="'.$strClass.'" id="#ITEM#_prop_'.$arProp['ID'].'_cont">'.
'<span class="bx_item_section_name_gray">'.htmlspecialcharsex($arProp['NAME']).'</span>'.
'<div class="bx_size_scroller_container"><div class="bx_size"><ul id="#ITEM#_prop_'.$arProp['ID'].'_list" style="width: '.$strWidth.';">';
				foreach ($arProp['VALUES'] as $arOneValue)
				{
					$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
					$templateRow .= '<li data-treevalue="'.$arProp['ID'].'_'.$arOneValue['ID'].'" data-onevalue="'.$arOneValue['ID'].'" style="width: '.$strOneWidth.';" title="'.$arOneValue['NAME'].'"><i></i><span class="cnt">'.$arOneValue['NAME'].'</span></li>';
				}
				$templateRow .= '</ul></div>'.
'<div class="bx_slide_left" id="#ITEM#_prop_'.$arProp['ID'].'_left" data-treevalue="'.$arProp['ID'].'" style="'.$strSlideStyle.'"></div>'.
'<div class="bx_slide_right" id="#ITEM#_prop_'.$arProp['ID'].'_right" data-treevalue="'.$arProp['ID'].'" style="'.$strSlideStyle.'"></div>'.
'</div></div>';
			}
			elseif ('PICT' == $arProp['SHOW_MODE'])
			{
				if (5 < $arProp['VALUES_COUNT'])
				{
					$strClass = 'bx_item_detail_scu full';
					$strWidth = ($arProp['VALUES_COUNT']*20).'%';
					$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
					$strSlideStyle = '';
				}
				else
				{
					$strClass = 'bx_item_detail_scu';
					$strWidth = '100%';
					$strOneWidth = '20%';
					$strSlideStyle = 'display: none;';
				}
				$templateRow .= '<div class="'.$strClass.'" id="#ITEM#_prop_'.$arProp['ID'].'_cont">'.
'<span class="bx_item_section_name_gray">'.htmlspecialcharsex($arProp['NAME']).'</span>'.
'<div class="bx_scu_scroller_container"><div class="bx_scu"><ul id="#ITEM#_prop_'.$arProp['ID'].'_list" style="width: '.$strWidth.';">';
				foreach ($arProp['VALUES'] as $arOneValue)
				{
					$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
					$templateRow .= '<li data-treevalue="'.$arProp['ID'].'_'.$arOneValue['ID'].'" data-onevalue="'.$arOneValue['ID'].'" style="width: '.$strOneWidth.'; padding-top: '.$strOneWidth.';"><i title="'.$arOneValue['NAME'].'"></i>'.
'<span class="cnt"><span class="cnt_item" style="background-image:url(\''.$arOneValue['PICT']['SRC'].'\');" title="'.$arOneValue['NAME'].'"></span></span></li>';
				}
				$templateRow .= '</ul></div>'.
'<div class="bx_slide_left" id="#ITEM#_prop_'.$arProp['ID'].'_left" data-treevalue="'.$arProp['ID'].'" style="'.$strSlideStyle.'"></div>'.
'<div class="bx_slide_right" id="#ITEM#_prop_'.$arProp['ID'].'_right" data-treevalue="'.$arProp['ID'].'" style="'.$strSlideStyle.'"></div>'.
'</div></div>';
			}
			$arSkuTemplate[$arProp['CODE']] = $templateRow;
		}
		unset($templateRow, $arProp);
	}

	if ($arParams["DISPLAY_TOP_PAGER"])
	{
		?><? echo $arResult["NAV_STRING"]; ?><?
	}

	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
?><!--<div class="bx_catalog_list_home col<? echo $arParams['LINE_ELEMENT_COUNT']; ?> <? echo $templateData['TEMPLATE_CLASS']; ?>">-->
<div class="product-box box-section">
	<div class="containers">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p>Сортировать 
					<select class="sort_select">
						<option value="<?=$arResult["SECTION_PAGE_URL"]?>?sort=name&method=asc" <?if ($_GET["sort"] == "name" && $_GET["method"] == "asc"):?> selected="selected" <?endif;?>>
							по названию (А-Я)
						</option>
						<option value="<?=$arResult["SECTION_PAGE_URL"]?>?sort=name&method=desc" <?if ($_GET["sort"] == "name" && $_GET["method"] == "desc"):?> selected="selected" <?endif;?>>
							по названию (Я-А)
						</option>
						<option value="<?=$arResult["SECTION_PAGE_URL"]?>?sort=catalog_PRICE_1&method=asc" <?if ($_GET["sort"] == "catalog_PRICE_1" && $_GET["method"] == "asc"):?> selected="selected" <?endif;?>>
							по цене (по возрастанию)
						</option>
						<option value="<?=$arResult["SECTION_PAGE_URL"]?>?sort=catalog_PRICE_1&method=desc" <?if ($_GET["sort"] == "catalog_PRICE_1" && $_GET["method"] == "desc"):?> selected="selected" <?endif;?>>
							по цене (по убыванию)
						</option>
					</select>
				</p>
			</div>
		</div>
		<div class="row">

<?
	$sections_pr=array(588, 611, 612, 613, 614, 615);
if (!in_array($arResult["ID"], $sections_pr)) {
	foreach ($arResult['ITEMS'] as $key => $arItem)
	{
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
		$strMainID = $this->GetEditAreaId($arItem['ID']);

		$arItemIDs = array(
			'ID' => $strMainID,
			'PICT' => $strMainID.'_pict',
			'SECOND_PICT' => $strMainID.'_secondpict',
			'STICKER_ID' => $strMainID.'_sticker',
			'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
			'QUANTITY' => $strMainID.'_quantity',
			'QUANTITY_DOWN' => $strMainID.'_quant_down',
			'QUANTITY_UP' => $strMainID.'_quant_up',
			'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
			'BUY_LINK' => $strMainID.'_buy_link',
			'BASKET_ACTIONS' => $strMainID.'_basket_actions',
			'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
			'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
			'COMPARE_LINK' => $strMainID.'_compare_link',

			'PRICE' => $strMainID.'_price',
			'DSC_PERC' => $strMainID.'_dsc_perc',
			'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',
			'PROP_DIV' => $strMainID.'_sku_tree',
			'PROP' => $strMainID.'_prop_',
			'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
			'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
		);

		$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

		$productTitle = (
			isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
			? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
			: $arItem['NAME']
		);
		$imgTitle = (
			isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
			? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
			: $arItem['NAME']
		);

		$minPrice = false;
		if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
			$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);

		?>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="product-item">
				<div class="product-cnt">
					<div class="product-item-img">
						<a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>">
							<img class="product-cnt-bg img-responsive grey" src="/bitrix/templates/beez/images/product-cnt-bg-grey.png" alt="" title="">
							<img class="product-cnt-bg img-responsive yellow" src="/bitrix/templates/beez/images/product-cnt-bg.png" alt="" title="">
							<div class="product-img">
								<img class=" img-responsive" src="<? echo $arItem['PREVIEW_PICTURE_S']['src']; ?>" alt="" title="">
								<div class="prev_text_abs"><?=$arItem['DETAIL_TEXT'];?></div>
							</div>
						</a>
					</div>
					<div class="product-title">
					<a class="link-c" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" title="<? echo $productTitle; ?>"><? echo $productTitle; ?></a>
					</div>
					<div class="product-price margin-bottom-sm margin-bottom-xs">
					<?
						if (!empty($minPrice) && ($minPrice['PRINT_DISCOUNT_VALUE']>0 || $minPrice['PRINT_VALUE']>0))
						{
							if ('N' == $arParams['PRODUCT_DISPLAY_MODE'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS']))
							{
								echo GetMessage(
									'CT_BCS_TPL_MESS_PRICE_SIMPLE_MODE',
									array(
										'#PRICE#' => $minPrice['PRINT_DISCOUNT_VALUE'],
										'#MEASURE#' => GetMessage(
											'CT_BCS_TPL_MESS_MEASURE_SIMPLE_MODE',
											array(
												'#VALUE#' => $minPrice['CATALOG_MEASURE_RATIO'],
												'#UNIT#' => $minPrice['CATALOG_MEASURE_NAME']
											)
										)
									)
								);
							}
							else
							{
								echo $minPrice['PRINT_DISCOUNT_VALUE'];
							}
							if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE'])
							{
								?> <span><? echo $minPrice['PRINT_VALUE']; ?></span><?
							}
						} else {
							?> <span>уточните</span><?
						}
						unset($minPrice);
					?>
					</div>
				</div>
			</div>
		</div><?
	}
} else {
	?>
	
	<div class="table-responsive">
	<table class="table table-bordered">
      <thead>
        <tr>
          <th>Фото</th>
          <th>Наименование</th>
          <th>Описание</th>
          <th>Цена</th>
          <th>Купить</th>
        </tr>
      </thead>
      <tbody>
        
	<?
	foreach ($arResult['ITEMS'] as $key => $arItem)
	{
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
		$strMainID = $this->GetEditAreaId($arItem['ID']);

		$arItemIDs = array(
			'ID' => $strMainID,
			'PICT' => $strMainID.'_pict',
			'SECOND_PICT' => $strMainID.'_secondpict',
			'STICKER_ID' => $strMainID.'_sticker',
			'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
			'QUANTITY' => $strMainID.'_quantity',
			'QUANTITY_DOWN' => $strMainID.'_quant_down',
			'QUANTITY_UP' => $strMainID.'_quant_up',
			'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
			'BUY_LINK' => $strMainID.'_buy_link',
			'BASKET_ACTIONS' => $strMainID.'_basket_actions',
			'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
			'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
			'COMPARE_LINK' => $strMainID.'_compare_link',

			'PRICE' => $strMainID.'_price',
			'DSC_PERC' => $strMainID.'_dsc_perc',
			'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',
			'PROP_DIV' => $strMainID.'_sku_tree',
			'PROP' => $strMainID.'_prop_',
			'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
			'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
		);

		$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

		$productTitle = (
			isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
			? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
			: $arItem['NAME']
		);
		$imgTitle = (
			isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
			? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
			: $arItem['NAME']
		);

		$minPrice = false;
		if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
			$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);

		?>
		<tr>
          <th class="pic_th">
				<a class="fancybox" href="<? echo $arItem['PREVIEW_PICTURE_SB']['src']; ?>">
					<img class=" img-responsive" src="<? echo $arItem['PREVIEW_PICTURE_S']['src']; ?>" alt="" title="">
					<div class="zoom">
					</div>
					<span class="glyphicon glyphicon-search"></span>
				</a>
		  </th>
          <td><a class="link-c" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" title="<? echo $productTitle; ?>"><? echo $productTitle; ?></a></td>
          <td><?=$arItem['DETAIL_TEXT']?></td>
          <td>		
			<?
				if (!empty($minPrice))
				{
					if ('N' == $arParams['PRODUCT_DISPLAY_MODE'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS']))
					{
						echo GetMessage(
							'CT_BCS_TPL_MESS_PRICE_SIMPLE_MODE',
							array(
								'#PRICE#' => $minPrice['PRINT_DISCOUNT_VALUE'],
								'#MEASURE#' => GetMessage(
									'CT_BCS_TPL_MESS_MEASURE_SIMPLE_MODE',
									array(
										'#VALUE#' => $minPrice['CATALOG_MEASURE_RATIO'],
										'#UNIT#' => $minPrice['CATALOG_MEASURE_NAME']
									)
								)
							)
						);
					}
					else
					{
						echo $minPrice['PRINT_DISCOUNT_VALUE'];
					}
					if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE'])
					{
						?> <span><? echo $minPrice['PRINT_VALUE']; ?></span><?
					}
				}
				unset($minPrice);
			?>
		   </td>
          <td><span class="basket addCartBtn" pid="<?=$arItem['ID']?>"></span>
		  
		  <div id="CatalogElementBasket_bx_117848907_2758" class="thanks_message_">
			  <table class="popup-window popup-window-titlebar" cellspacing="0">
				  <tbody>
					  <tr class="popup-window-top-row">
						  <td class="popup-window-left-column">
							<div class="popup-window-left-spacer"></div>
						  </td>
						  <td class="popup-window-center-column">
							<div class="popup-window-titlebar" id="popup-window-titlebar-CatalogElementBasket_bx_117848907_2758">
								<div style="margin-right: 30px; white-space: nowrap;">Товар добавлен в корзину</div>
							</div>
						  </td>
						  <td class="popup-window-right-column"><div class="popup-window-right-spacer"></div></td>
					  </tr>
					  <tr class="popup-window-content-row">
						  <td class="popup-window-left-column"></td>
						  <td class="popup-window-center-column">
							  <div class="popup-window-content" id="popup-window-content-CatalogElementBasket_bx_117848907_2758">
								  <div style="width: 96%; margin: 10px 2%; text-align: center;">
									  <img src="<?=$arItem['PREVIEW_PICTURE_S']['src'];?>" height="130">
									  <p><?=$arItem['NAME'];?></p>
								  </div>
							  </div>
							  <div class="popup-window-hr popup-window-buttons-hr">
								<i></i>
							  </div>
							  <div class="popup-window-buttons">
								  <span class="bx_item_detail bx_yellow">
									<a class="bx_medium bx_bt_button" href="/personal/cart/">Перейти в корзину</a>
								  </span>
							  </div>
						  </td>
						  <td class="popup-window-right-column"></td>
					  </tr>
					  <tr class="popup-window-bottom-row">
						  <td class="popup-window-left-column"></td>
						  <td class="popup-window-center-column"></td>
						  <td class="popup-window-right-column"></td>
					  </tr>
				  </tbody>
			  </table>
			  <span class="popup-window-close-icon popup-window-titlebar-close-icon closer" style="top: 10px; right: 10px;"></span>
		  </div>
		  </td>
        </tr>
		<?
	}
	?>
      </tbody>
	</table>
	</div>
	 
	<?
}
?><div style="clear: both;"></div>
</div></div></div>
<script type="text/javascript">
BX.message({
	BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
	BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
	ADD_TO_BASKET_OK: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
	TITLE_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_ERROR') ?>',
	TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_BASKET_PROPS') ?>',
	TITLE_SUCCESSFUL: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
	BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
	BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
	BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE') ?>',
	BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
	COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_OK') ?>',
	COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
	COMPARE_TITLE: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE') ?>',
	BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
	SITE_ID: '<? echo SITE_ID; ?>'
});
</script>
<?
	if ($arParams["DISPLAY_BOTTOM_PAGER"])
	{
		?><? echo $arResult["NAV_STRING"]; ?><?
	}
} else {
	echo "<div class='no-items'>К сожалению, ничего не найдено.</div>";
}
?>
<script>
	$(document).ready(function(){
		$(".sort_select").on( "selectmenuchange", function( event, ui ) {
			window.location=$(".sort_select").find("option:selected").val();
		} );
	/* 	$(".fancybox").hover(function(){
			$(this).find("div, span").show();
		},
		function(){
			$(this).find("div, span").hide();
		}); */
		$(function() {
			var offset = $(".bx_sidebar").offset(),
				nav_offset = $("#navigation").offset(),
				height = document.body.clientHeight;
			var topPadding = 15;
			$(window).scroll(function() {
				if ($(window).scrollTop() > offset.top && height>$(".bx_sidebar").height()) {
					if ($(window).scrollTop() > nav_offset.top-$(".bx_sidebar").height()) {
						$(".bx_sidebar").stop().animate({marginTop: $(".bx_sidebar").css("margin-top")});
					}
					else {
						$(".bx_sidebar").stop().animate({marginTop: $(window).scrollTop() - offset.top + topPadding});
					}
				}
				else { 
						$(".bx_sidebar").stop().animate({marginTop: 0});
				};
			});
		});
	});
</script>
<script>
	$().ready(function() {

	});
</script>