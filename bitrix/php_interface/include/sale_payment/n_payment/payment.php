<?require($_SERVER['DOCUMENT_ROOT']."/bitrix/header.php");?>
<?
if (!CModule::IncludeModule("sale")) return;
/* $fields="type=F112EP_COD_SINGLE&forms=F112EP_COD_SINGLE&Summ=100000&Sender=1&SenderAddress=2&SenderIndex=333333&Recipient=4&RecipientAddress=5&RecipientIndex=666666&RecipientCashOffice=true&RecipientCash=&RecipientINN=&RecipientCorrespondentAccount=&RecipientBankName=&RecipientCheckingAccount=&RecipientBIK=&RecipientSms=&RecipientPhone=&sum=1000";
$headers = array(    "Accept: application/xml", 
					 "Accept-Encoding:gzip",
                     "Content-Type: application/x-www-form-urlencoded;charset=UTF-8");

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://www.pochta.ru/portal-pdf-form/restful/blank/generate");
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_REFERER, 'https://www.pochta.ru/form?type=F112EP_COD_SINGLE'); */
/* curl_setopt($ch, CURLOPT_FAILONERROR, 1);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); */
/* curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POST, 1); 
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields); 
curl_setopt($ch, CURLOPT_ENCODING , "gzip"); */
/* curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36 OPR/35.0.2066.68");
curl_setopt($ch, CURLOPT_TIMEOUT, 100);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 100);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); 
curl_setopt($ch, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'].'/cookie.txt'); // сохранять куки в файл
curl_setopt($ch, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'].'/cookie.txt');      */   
 
	 
/* $page = array();
$page['html'] = curl_exec($ch);
$page['err'] = curl_errno($ch);
$page['errmsg'] = curl_error($ch);
$page['header'] = curl_getinfo($ch);
curl_close($ch);
print("<pre>");
print_r($page);
print_r($url_root);
print("</pre>"); */

/* $data = iconv('CP1251', 'UTF-8', curl_exec($ch));
curl_close($ch);
$destination = dirname(__FILE__) . '/file.pdf';
$file = fopen($destination, "w+");
fputs($file, $data);
fclose($file);
$filename = 'google.pdf';

header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Disposition: attachment; filename=$filename");
header("Content-Type: application/pdf");
header("Content-Transfer-Encoding: binary");
readfile($destination); */
?>

<script>
/* 	$.ajax({
		url: "https://www.pochta.ru/portal-pdf-form/restful/blank/generate",
		type: "POST",
		data: "type=F112EP_COD_SINGLE&forms=F112EP_COD_SINGLE&Summ=100000&Sender=1&SenderAddress=2&SenderIndex=333333&Recipient=4&RecipientAddress=5&RecipientIndex=666666&RecipientCashOffice=true&RecipientCash=1&RecipientINN=1&RecipientCorrespondentAccount=1&RecipientBankName=1&RecipientCheckingAccount=1&RecipientBIK=1&RecipientSms=1&RecipientPhone=1&sum=1000",
		dataType:"html",
		async: false,
		success: function(data){
			console.log(data);
			console.log("answer");
		},
		error: function(jqxhr, status, errorMsg) {
			alert(jqxhr+"  "+status+"  "+errorMsg);
		}

	}); */
</script>
<div class="container">
	<div class="info_block">
	<div class="row">
		<div class="col-sm-3 col-xs-12">
			<div class="n_cnt">
				<div class="n_title">
				Название магазина
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-xs-12">
			<div class="n_cnt">
				<div class="n_value">
				<? echo CSalePaySystemAction::GetParamValue("SELLER_NAME"); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3 col-xs-12">
			<div class="n_cnt">
				<div class="n_title">
				Адрес магазина
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-xs-12">
			<div class="n_cnt">
				<div class="n_value">
				<? echo CSalePaySystemAction::GetParamValue("SELLER_ADDRESS"); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3 col-xs-12">
			<div class="n_cnt">
				<div class="n_title">
				Почтовый индекс
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-xs-12">
			<div class="n_cnt">
				<div class="n_value">
				<? echo CSalePaySystemAction::GetParamValue("SELLER_ZIP"); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3 col-xs-12">
			<div class="n_cnt">
				<div class="n_title">
				ИНН продавца
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-xs-12">
			<div class="n_cnt">
				<div class="n_value">
				<? echo CSalePaySystemAction::GetParamValue("SELLER_INN"); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3 col-xs-12">
			<div class="n_cnt">
				<div class="n_title">
				Наименование банка
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-xs-12">
			<div class="n_cnt">
				<div class="n_value">
				<? echo CSalePaySystemAction::GetParamValue("SELLER_BANK_NAME"); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3 col-xs-12">
			<div class="n_cnt">
				<div class="n_title">
				БИК банка
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-xs-12">
			<div class="n_cnt">
				<div class="n_value">
				<? echo CSalePaySystemAction::GetParamValue("SELLER_BANK_BIK"); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3 col-xs-12">
			<div class="n_cnt">
				<div class="n_title">
				Корсчет банка
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-xs-12">
			<div class="n_cnt">
				<div class="n_value">
				<? echo CSalePaySystemAction::GetParamValue("SELLER_BANK_KOR"); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3 col-xs-12">
			<div class="n_cnt">
				<div class="n_title">
				Расчетный счет
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-xs-12">
			<div class="n_cnt">
				<div class="n_value">
				<? echo CSalePaySystemAction::GetParamValue("SELLER_ACC"); ?>
				</div>
			</div>
		</div>
	</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="n_cnt">
				<a class="bx_bt_button to_pay" href="https://www.pochta.ru/form?type=F112EP_COD_SINGLE" target="_blank">Нажмите для перехода на официальный сайт Почты России для генерации бланка наложенного платежа <span>(ссылка откроется в новом окне)</span></a>
			</div>
		</div>
	</div>
</div>
<style>
.info_block {
    margin-bottom: 40px;
}
.n_title {
    font-weight: bold;
}
a.bx_bt_button.to_pay span {
    display: block;
}
.n_cnt a {
    text-align: center;
}
</style>
<?require($_SERVER['DOCUMENT_ROOT']."/bitrix/footer.php");?>