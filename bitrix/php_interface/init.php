<?
// подписываемся на событие CurrencyFormat модуля валют.
// вызывается в функции \CAllCurrencyLang::CurrencyFormat
$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandlerCompatible('currency', 'CurrencyFormat',
    array('CCurrencyLangHandler', 'CurrencyFormat'));
 
 
// код класса
class CCurrencyLangHandler
{
    public static function CurrencyFormat($price, $currency)
    {
        if (!(defined('ADMIN_SECTION') && true === ADMIN_SECTION)) {
            return sprintf('%s <span class="price">⃏</span>', number_format($price, 0, ' ', ' '));
        }
    }
}

?>