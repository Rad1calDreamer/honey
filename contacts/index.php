<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Контакты");
$APPLICATION->SetTitle("Контакты");
?><div class="row contact-intro">
<div class="col-md-12">
Приглашаем всех костромичей заходить к нам почаще и брать немного солнца и меда, получать заряд молодости, здоровья и красоты!
</div>
</div>
<div class="row contacts">
<div class="col-md-4">
		Адрес: 156000, Кострома, улица Ленина, 8.
	</div>
	<div class="col-md-4">
		Телефон: +7 (4942) 31-50-12
	</div>
	<div class="col-md-4">
		E-mail: info@honey-sale.ru
	</div>
</div>
<div class="row">
<div class="col-md-12">
<? Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("map"); ?>
<?$APPLICATION->IncludeComponent("bitrix:map.yandex.view", "map", Array(
	"COMPONENT_TEMPLATE" => ".default",
		"INIT_MAP_TYPE" => "MAP",	// Стартовый тип карты
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:57.77097488532363;s:10:\"yandex_lon\";d:40.928174812634396;s:12:\"yandex_scale\";i:15;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:40.9291332008;s:3:\"LAT\";d:57.769898661;s:4:\"TEXT\";s:41:\"Магазин \"Пчеловодство\"\";}}}",	// Данные, выводимые на карте
		"MAP_WIDTH" => "100%",	// Ширина карты
		"MAP_HEIGHT" => "300",	// Высота карты
		"CONTROLS" => array(	// Элементы управления
			0 => "ZOOM",
			1 => "MINIMAP",
			2 => "TYPECONTROL",
			3 => "SCALELINE",
		),
		"OPTIONS" => array(	// Настройки
			0 => "ENABLE_SCROLL_ZOOM",
			1 => "ENABLE_DBLCLICK_ZOOM",
			2 => "ENABLE_DRAGGING",
		),
		"MAP_ID" => "",	// Идентификатор карты
	),
	false
);?>
<? Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("map", ""); ?>
</div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>