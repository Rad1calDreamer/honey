<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("");
$APPLICATION->SetPageProperty("keywords", "мёд, пчеловодство, пчёлы, продукты пчеловодства");
$APPLICATION->SetPageProperty("title", "Пчеловодство - интернет-магазин в Костроме"); ?>

    <div class="mainPageSlider">
        <div class="container">
            <div class="sliderWrap">
                <?
                $arFilter = array('IBLOCK_ID' => 4, 'ACTIVE' => 'Y');
                $arSelect = array('ID', 'PREVIEW_PICTURE');
                $arResult = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
                while ($arItem = $arResult->Fetch()) {
                    ?>
                    <div class="mainPageSliderItem">
                        <a href="/catalog">
                            <img src="<?= CFile::GetPath($arItem['PREVIEW_PICTURE']); ?>" alt="">
                        </a>
                    </div>
                <? } ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="mainPageSmallTeaser">
            <div class="item it1"><span>25 видов мёда и более 500 товаров для здоровья</span></div>
            <div class="item it2"><span>Отличные цены на мёд
            от 200 рублей за 1 кг</span>
            </div>
            <div class="item it3"><span>Свои пасеки, лабороторный контроль качества мёда</span></div>
            <div class="item it4"><span>Собственное производство, работаем с 1961 года</span></div>
        </div>

        <div class="mainPageCatalogTeaser">
            <div class="item"><img src="/_images/catalogTeaser1.jpg" alt="">
                <div class="desc">
                    <div class="text">
                        <p class="name">Мед</p>
                        <p class="price">от 200 р.</p>
                    </div>
                </div>
            </div>
            <div class="item"><img src="/_images/catalogTeaser2.jpg" alt="">
                <div class="desc">
                    <div class="text">
                        <p class="name">Мед в подарок</p>
                        <p class="price">от 225 р.</p>
                    </div>
                </div>
            </div>
            <div class="item"><img src="/_images/catalogTeaser3.jpg" alt="">
                <div class="desc">
                    <div class="text">
                        <p class="name">продукты пчеловодства</p>
                        <p class="price">от 200 р.</p>
                    </div>
                </div>
            </div>
            <div class="item"><img src="/_images/catalogTeaser4.jpg" alt="">
                <div class="desc">
                    <div class="text">
                        <p class="name">медовые напитки</p>
                        <p class="price">от 150 р.</p>
                    </div>
                </div>
            </div>
            <div class="item"><img src="/_images/catalogTeaser5.jpg" alt="">
                <div class="desc">
                    <div class="text">
                        <p class="name">товары для пчеловодов</p>
                        <p class="price">от 800 р.</p>
                    </div>
                </div>
            </div>
            <div class="item"><img src="/_images/catalogTeaser6.jpg" alt="">
                <div class="desc">
                    <div class="text">
                        <p class="name">товары для здоровья</p>
                        <p class="price">от 150 р.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="orangeButton link centered">
            <a href="/catalog">ПЕРЕЙТИ В КАТАЛОГ</a>
        </div>

        <h2>Хит продаж</h2>
        <div class="hs-catalogContainer slider">
            <?
            if (!CModule::IncludeModule('sale')) return;
            $yvalue = 2;
            $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "DETAIL_TEXT", "DETAIL_PAGE_URL", "DATE_ACTIVE_FROM", "SHOW_COUNTER", "PROPERTY_*");
            $res = CIBlockElement::GetList(Array("sort" => "DESC"), $arFilter, false, Array("nPageSize" => 12), $arSelect);
            while ($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $arProps = $ob->GetProperties();
                $price = CPrice::GetBasePrice($arFields['ID']);
                if (empty($price)) {
                    $arSelect_o = Array("ID", "IBLOCK_ID");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
                    $arFilter_o = Array("IBLOCK_ID" => 3, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "PROPERTY_CML2_LINK" => $arFields['ID']);
                    $res_o = CIBlockElement::GetList(Array(), $arFilter_o, false, false, $arSelect_o);
                    $ids = array();
                    while ($ob_o = $res_o->GetNextElement()) {
                        $arFields_o = $ob_o->GetFields();
                        $ids[] = $arFields_o["ID"];
                    }
                    $price = CPrice::GetList(array(), array('PRODUCT_ID' => $ids), array('MAX' => "PRICE"))->Fetch();
                }
                ?>

                <div class="hs-catalogItem">
                    <div class="hs-catalogItemImage">
                        <a href="<?= $arFields['DETAIL_PAGE_URL']; ?>">
                            <img src="<?= CFile::GetPath($arFields['PREVIEW_PICTURE']) ?>"
                                 class="" alt="" title="">
                        </a>
                    </div>
                    <div class="hs-product-title">
                        <a class="link-c"
                           href="<?= $arFields['DETAIL_PAGE_URL']; ?>"><?= $arFields['NAME']; ?></a>
                    </div>
                    <? $av = $arProps['NOT_AV']['VALUE']; ?>
                    <div class="product-available <?= ($av) ? 'note_av' : 'av'; ?>">
                        <? if ($av) { ?>
                            Нет в наличии
                        <?
                        } else { ?>
                            Есть в наличии
                        <?
                        } ?>
                    </div>

                    <div class="hs-product-price">от
                        <?= substr($price['PRICE'], 0, strpos($price['PRICE'], '.')); ?> <span
                            class="price">⃏</span>
                    </div>
                </div>
                <?
            }
            ?>
        </div>

    </div>


    <div class="best-box box-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="title-section text-center-sm">
                        <span class="ico-best ico-title-section hidden-xs"></span>Популярные товары
                    </div>
                </div>
            </div>
        </div>
        <div class="best-product-box box-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bee-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="bee-one">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="comments-box bg-site box-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="title-section text-center-sm">
                        Отзывы наших покупателей
                    </div>
                </div>
            </div>
            <div class="comment-item">
                <? $APPLICATION->IncludeComponent(
                    "bisexpert:owlslider",
                    "main_r",
                    Array(
                        "AUTO_HEIGHT" => "Y",
                        "AUTO_PLAY" => "Y",
                        "AUTO_PLAY_SPEED" => "5000",
                        "CACHE_TIME" => "3600",
                        "CACHE_TYPE" => "A",
                        "COMPONENT_TEMPLATE" => ".default",
                        "COMPOSITE" => "Y",
                        "COUNT" => "8",
                        "DISABLE_LINK_DEV" => "Y",
                        "DRAG_BEFORE_ANIM_FINISH" => "Y",
                        "ENABLE_JQUERY" => "N",
                        "ENABLE_OWL_CSS_AND_JS" => "Y",
                        "HEIGHT_RESIZE" => "",
                        "IBLOCK_ID" => "5",
                        "IBLOCK_TYPE" => "reviews",
                        "IMAGE_CENTER" => "Y",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "IS_PROPORTIONAL" => "Y",
                        "ITEMS_SCALE_UP" => "N",
                        "LINK_URL_PROPERTY_ID" => "0",
                        "MAIN_TYPE" => "iblock",
                        "MOUSE_DRAG" => "Y",
                        "NAVIGATION" => "Y",
                        "NAVIGATION_TYPE" => "arrows",
                        "PAGINATION" => "N",
                        "PAGINATION_NUMBERS" => "N",
                        "PAGINATION_SPEED" => "1000",
                        "RANDOM" => "N",
                        "RANDOM_TRANSITION" => "N",
                        "RESPONSIVE" => "Y",
                        "REWIND_SPEED" => "1200",
                        "SCROLL_COUNT" => "1",
                        "SECTION_ID" => "0",
                        "SHOW_DESCRIPTION_BLOCK" => "N",
                        "SLIDE_SPEED" => "800",
                        "SORT_DIR_1" => "asc",
                        "SORT_DIR_2" => "asc",
                        "SORT_FIELD_1" => "id",
                        "SORT_FIELD_2" => "",
                        "SPECIAL_CODE" => "unic_r",
                        "STOP_ON_HOVER" => "Y",
                        "TEXT_PROPERTY_ID" => "0",
                        "TOUCH_DRAG" => "Y",
                        "TRANSITION_TYPE_FOR_ONE_ITEM" => "default",
                        "WIDTH_RESIZE" => ""
                    )
                ); ?>
            </div>
        </div>
    </div>
    <div class="news-box box-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-section text-center-sm">
                        <span class="ico-news ico-title-section hidden-xs"></span>Новинки
                    </div>
                </div>
            </div>
        </div>
        <div class="news-product-box box-section">
            <div class="container">
                <div class="row">
                    <?
                    $yvalue = 2;
                    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "DETAIL_TEXT", "DETAIL_PAGE_URL", "DATE_ACTIVE_FROM", "PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
                    $arFilter = Array("IBLOCK_ID" => IntVal($yvalue), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "PROPERTY_NEWPRODUCT_VALUE" => "да");
                    $res = CIBlockElement::GetList(Array("ID" => "ASC"), $arFilter, false, Array("nPageSize" => 4), $arSelect);
                    while ($ob = $res->GetNextElement()) {
                        $arFields = $ob->GetFields();
                        $arProps = $ob->GetProperties();
                        /* print("<pre>");
                        print_r($arFields);
                        print_r($arProps['NEWPRODUCT']);
                        print("</pre>");  */
                        $price = CPrice::GetBasePrice($arFields['ID']);
                        if (empty($price)) {
                            $arSelect_o = Array("ID", "IBLOCK_ID");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
                            $arFilter_o = Array("IBLOCK_ID" => 3, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "PROPERTY_CML2_LINK" => $arFields['ID']);
                            $res_o = CIBlockElement::GetList(Array(), $arFilter_o, false, false, $arSelect_o);
                            $ids = array();
                            while ($ob_o = $res_o->GetNextElement()) {
                                $arFields_o = $ob_o->GetFields();
                                $ids[] = $arFields_o["ID"];
                            }
                            $price = CPrice::GetList(array(), array('PRODUCT_ID' => $ids), array('MAX' => "PRICE"))->Fetch();
                        }
                        ?>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="product-item">
                                <div class="product-cnt">
                                    <div class="product-item-img">
                                        <a href="<?= $arFields['DETAIL_PAGE_URL']; ?>"> <img
                                                src="/bitrix/templates/beez/images/product-cnt-bg-grey.png"
                                                class="product-cnt-bg img-responsive grey" alt="" title=""> <img
                                                src="/bitrix/templates/beez/images/product-cnt-bg.png"
                                                class="product-cnt-bg img-responsive yellow" alt="" title="">
                                            <div class="product-img">
                                                <img src="<?= CFile::GetPath($arFields['PREVIEW_PICTURE']) ?>"
                                                     class="main_img_prod img-responsive" alt="" title="">
                                                <div class="prev_text_abs">
                                                    <?= $arFields['DETAIL_TEXT']; ?>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="product-title">
                                        <a class="link-c"
                                           href="<?= $arFields['DETAIL_PAGE_URL']; ?>"><?= $arFields['NAME']; ?></a>
                                    </div>
                                    <div class="product-price margin-bottom-sm margin-bottom-xs">
                                        <?= substr($price['PRICE'], 0, strpos($price['PRICE'], '.')); ?> <span
                                            class="price">⃏</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $(".subscribe_btn").click(function () {

                var email = $("#subscribe").val();

                if (email == "" || email.replace(/\s*/g, '') == "") {
                    $("#subscribe").addClass("error_input");
                }
                else {
                    var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
                    if (!pattern.test(email)) {
                        $("#subscribe").addClass("error_input");
                    }
                    else {
                        $("#subscribe").removeClass("error_input");
                    }
                }
                //alert($(this).parents(".subscribe-box").find(".error_input")[0]);
                if (!$(this).parents(".subscribe-box").find(".error_input")[0]) {
                    $.ajax({
                        url: "/ajax.handler.php",
                        type: "POST",
                        data: "PAGE=subscribe&email=" + email,
                        dataType: "html",
                        async: false,
                        success: function (data) {
                            if (data.length > 0) {
                                if (data == 'Адрес подписки уже существует. Пожалуйста, укажите другой адрес.<br>') $("#subscribe").attr("placeholder", 'Адрес подписки уже существует.');
                                else  $("#subscribe").attr("placeholder", 'Произошла непредвиденная ошибка');
                                $("#subscribe").val('');
                                $("#subscribe").addClass("error_message");
                                $(".thanks_subscribe").hide();
                            }
                            else {
                                $("#subscribe").val('');
                                $("#subscribe").attr("placeholder", 'Введите Ваш e-mail...');
                                $(".subscribe-box-form div").hide();
                                $(".thanks_subscribe").show('slow');
                                $("#subscribe").removeClass("error_message");

                            }
                        },
                        error: function (jqxhr, status, errorMsg) {
                            alert(jqxhr + "  " + status + "  " + errorMsg);
                        }

                    });
                }
            });
        })
    </script>
    <div class="subscribe-box box-section">
        <div class="container">
            <div class="row subscribe-box-form text-center-sm">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <span class="entity-margin-top-bottom">Будь в курсе всех специальных предложений</span>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-9 col-xs-12">
                    <input type="text" class="form-control subscribe" id="subscribe"
                           placeholder="Введите Ваш e-mail...">
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                    <button type="button" class="btn btn-success btn-white-xs subscribe_btn">Подписаться</button>
                </div>
                <div class="thanks_subscribe" style="display:none">
                    Спасибо за проявленый интерес! Ваша подписка оформлена.
                </div>
            </div>
        </div>
    </div>
    <div class="box-section">
        <div class="container seo-box ">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
 <span class="welcome">Добро пожаловать на страничку самого<br>
				 вкусного магазина «Пчеловодство»</span>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="bee-two hidden-xs">
                    </div>
                    <a href="/catalog/" class="btn btn-primary hidden-xs">Перейти в каталог</a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?
                    if (!CModule::IncludeModule("iblock")) return;

                    $arSelect = Array("*");
                    $arFilter = Array("IBLOCK_ID" => 9, "ID" => 3222, "ACTIVE" => "Y");
                    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1), $arSelect);

                    while ($ob = $res->GetNextElement()) {
                        $arFields = $ob->GetFields();
                    }
                    echo "<div class='service_text'>" . $arFields['DETAIL_TEXT'] . "</div>";
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="box-section">
        <div class="container partners-box ">
            <div class="row">
                <div class="col-xs-12">
                    <div class="title-section text-center-sm">
                        Наши партнёры
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <? $APPLICATION->IncludeComponent(
                        "bisexpert:owlslider",
                        "",
                        Array(
                            "AUTO_HEIGHT" => "Y",
                            "AUTO_PLAY" => "Y",
                            "AUTO_PLAY_SPEED" => "5000",
                            "CACHE_TIME" => "3600",
                            "CACHE_TYPE" => "A",
                            "COMPONENT_TEMPLATE" => ".default",
                            "COMPOSITE" => "Y",
                            "COUNT" => "20",
                            "DISABLE_LINK_DEV" => "Y",
                            "DRAG_BEFORE_ANIM_FINISH" => "Y",
                            "ENABLE_JQUERY" => "N",
                            "ENABLE_OWL_CSS_AND_JS" => "Y",
                            "HEIGHT_RESIZE" => "",
                            "IBLOCK_ID" => "10",
                            "IBLOCK_TYPE" => "partners",
                            "IMAGE_CENTER" => "Y",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "IS_PROPORTIONAL" => "Y",
                            "ITEMS_SCALE_UP" => "N",
                            "LINK_URL_PROPERTY_ID" => "0",
                            "MAIN_TYPE" => "iblock",
                            "MOUSE_DRAG" => "Y",
                            "NAVIGATION" => "Y",
                            "NAVIGATION_TYPE" => "arrows",
                            "PAGINATION" => "N",
                            "PAGINATION_NUMBERS" => "N",
                            "PAGINATION_SPEED" => "1000",
                            "RANDOM" => "N",
                            "RANDOM_TRANSITION" => "N",
                            "RESPONSIVE" => "Y",
                            "REWIND_SPEED" => "1200",
                            "SCROLL_COUNT" => "4",
                            "SECTION_ID" => "0",
                            "SHOW_DESCRIPTION_BLOCK" => "N",
                            "SLIDE_SPEED" => "800",
                            "SORT_DIR_1" => "asc",
                            "SORT_DIR_2" => "asc",
                            "SORT_FIELD_1" => "id",
                            "SORT_FIELD_2" => "",
                            "SPECIAL_CODE" => "unic_p",
                            "STOP_ON_HOVER" => "Y",
                            "TEXT_PROPERTY_ID" => "0",
                            "TOUCH_DRAG" => "Y",
                            "TRANSITION_TYPE_FOR_ONE_ITEM" => "default",
                            "WIDTH_RESIZE" => ""
                        )
                    ); ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $(".where_btn").click(function () {

                var email = $("#whereInputEmail").val(),
                    name = $("#whereInputName").val(),
                    order = $("#whereInputOrder").val(),
                    phone = $("#whereInputPhone").val();

                if (name == "" || name.replace(/\s*/g, '') == "") {
                    $("#whereInputName").addClass("error_input");
                }
                else {
                    $("#whereInputName").removeClass("error_input");
                }

                if (order == "" || order.replace(/\s*/g, '') == "") {
                    $("#whereInputOrder").addClass("error_input");
                }
                else {
                    $("#whereInputOrder").removeClass("error_input");
                }

                if (email == "" || email.replace(/\s*/g, '') == "") {
                    $("#whereInputEmail").addClass("error_input");
                }
                else {
                    var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
                    if (!pattern.test(email)) {
                        $("#whereInputEmail").addClass("error_input");
                    }
                    else {
                        $("#whereInputEmail").removeClass("error_input");
                    }
                }

                if (phone == "" || phone.replace(/\s*/g, '') == "") {
                    $("#whereInputPhone").addClass("error_input");
                }
                else {
                    var pattern = /^[0-9()\-+ ]+$/;
                    if (!pattern.test(phone)) {
                        $("#whereInputPhone").addClass("error_input");
                    }
                    else {
                        $("#whereInputPhone").removeClass("error_input");
                    }
                }

                if (!$(".error_input")[0]) {
                    $.ajax({
                        url: "/ajax.handler.php",
                        type: "POST",
                        data: "PAGE=where&email=" + email + "&name=" + name + "&order=" + order + "&phone=" + phone,
                        dataType: "html",
                        async: false,
                        success: function (data) {
                            $("#whereInputEmail").val('');
                            $("#whereInputName").val('');
                            $("#whereInputOrder").val('');
                            $("#whereInputPhone").val('');
                            //$("#where_form").hide('slow');
                            $(".thanks_where").show('slow');
                        },
                        error: function (jqxhr, status, errorMsg) {
                            alert(jqxhr + "  " + status + "  " + errorMsg);
                        }

                    });
                }
            });
        })
    </script> <!-- Modal -->
    <div class="modal fade" id="whereModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="title-con">
                    Заполните форму,<br>
                    и мы свяжемся с Вами.
                </div>
                <form class="form-inline" id="where_form" role="form">
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="text" class="form-control where_name" id="whereInputName" placeholder="ФИО">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="text" class="form-control where_phone" id="whereInputPhone"
                                   placeholder="Телефон">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="text" class="form-control where_order" id="whereInputOrder"
                                   placeholder="Номер заказа">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="text" class="form-control where_email" id="whereInputEmail"
                                   placeholder="Email">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 w250">
                            <button type="button" class="btn btn-success where_btn">Узнать</button>
                        </div>
                    </div>
                </form>
                <div class="thanks_where" style="display:none">
                    Спасибо! В ближайшее время наш менеджер свяжется с вами.
                </div>
            </div>
        </div>
    </div>
    <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>