$(document).ready(function () {
    $('.mainPageSlider .sliderWrap').owlCarousel({
        responsive: false,
        singleItem: true,
        items: 1
    });
    $('.hs-catalogContainer.slider').owlCarousel({
        items: 4,
        autoHeight: true,
        afterInit: function () {
            console.log(this);
            var height = this.$owlWrapper.height();
            $.each(this.$owlItems,function(i,n){
                $(n).find('.hs-catalogItem').innerHeight(height);
            })
        }
    });
});