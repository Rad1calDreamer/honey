<?require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php"); 
if($_POST["PAGE"] == "where"){
	require_once($_SERVER["DOCUMENT_ROOT"] . "/handlers/where.php");
}
if($_POST["PAGE"] == "call"){
	require_once($_SERVER["DOCUMENT_ROOT"] . "/handlers/call.php");
}
if($_POST["PAGE"] == "message"){
	require_once($_SERVER["DOCUMENT_ROOT"] . "/handlers/message.php");
}
if($_POST["PAGE"] == "subscribe"){
	require_once($_SERVER["DOCUMENT_ROOT"] . "/handlers/subscribe.php");
}
if($_POST["PAGE"] == "add_cart"){
	require_once($_SERVER["DOCUMENT_ROOT"] . "/handlers/add_cart.php");
}
if($_POST["PAGE"] == "review"){
	require_once($_SERVER["DOCUMENT_ROOT"] . "/handlers/review.php");
}